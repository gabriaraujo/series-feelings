var classModelImpl =
[
    [ "ModelImpl", "classModelImpl.html#a081505846c37ce9928f2176d77db4bc8", null ],
    [ "~ModelImpl", "classModelImpl.html#a427f422a6d356b94afbe3937d6452a2b", null ],
    [ "createComment", "classModelImpl.html#a5717dbb35cbca7d68b9b682bb4028dc5", null ],
    [ "createSerie", "classModelImpl.html#a67b5289aea32358df7d7361270797739", null ],
    [ "createUser", "classModelImpl.html#a45c36278de97fae8ba0c20cd25aac671", null ],
    [ "getSerieList", "classModelImpl.html#a058f7375b1ea2216a053fedea6fffa88", null ],
    [ "getUser", "classModelImpl.html#ac098ad97916ff988d518ded5056dabb0", null ],
    [ "getUserList", "classModelImpl.html#a46f441c1ba57cc395e6732cab80e78fd", null ],
    [ "readComments", "classModelImpl.html#af996e3a1067cb3383c53aec7337af68a", null ],
    [ "readSeries", "classModelImpl.html#a6d14c556105d6d998a27098270fecc48", null ],
    [ "readUser", "classModelImpl.html#a132c46a2d6ca86c7374a065b3079fe15", null ],
    [ "setSerieList", "classModelImpl.html#adf273dc18016a71a3834235ffc34330a", null ],
    [ "setUser", "classModelImpl.html#a7b012a2f457eb5bb06182198209312e5", null ],
    [ "setUserList", "classModelImpl.html#abe86a822ce647301fb429b713ec42495", null ],
    [ "writeComments", "classModelImpl.html#a17ee3cf131a05a31aec0c09dbf67e7b3", null ],
    [ "writeUser", "classModelImpl.html#a42b5948a513af3e20645cdb63fb6be76", null ],
    [ "serie_list", "classModelImpl.html#a9dfbca38669e22ce3a9a6e26d63f5b0e", null ],
    [ "user", "classModelImpl.html#a76f8d6f7634f9395c2576a5d11628e13", null ],
    [ "user_list", "classModelImpl.html#a5f4fa30abed8b4d4687bb7728d258b75", null ]
];