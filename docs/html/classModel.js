var classModel =
[
    [ "~Model", "classModel.html#af032d8433c87a0a3a431faf6563a1f03", null ],
    [ "createComment", "classModel.html#abe65897e450242de616ae6bb9a8036f7", null ],
    [ "createSerie", "classModel.html#a1939b08e140fa22ad357c0d11dc520f6", null ],
    [ "createUser", "classModel.html#aa2b7e29a0730382c3af1c875f168ca6a", null ],
    [ "getSerieList", "classModel.html#ad481ea688efe1ccbf26e05ad3ca12ca0", null ],
    [ "getUser", "classModel.html#a20cb9f90df3f8f235dc14fbc13b64167", null ],
    [ "getUserList", "classModel.html#a3e27c021561cb19ce48d4a456e75d5ab", null ],
    [ "readComments", "classModel.html#a500a81d76197bf1a0c6a088a34ffa3d6", null ],
    [ "readSeries", "classModel.html#a814142f468ebca45b0ef14b6b922397a", null ],
    [ "readUser", "classModel.html#ad3fa273a7dc1397ce748c657e552d6cc", null ],
    [ "setSerieList", "classModel.html#afbae8ee14036e91fc53a771361c41c80", null ],
    [ "setUser", "classModel.html#aae120afc85d42a7bedee5405911986a5", null ],
    [ "setUserList", "classModel.html#abe547831f265d0e25e1b026eb33e6770", null ],
    [ "writeComments", "classModel.html#a034e2b07214b9322374b677e636a83f7", null ],
    [ "writeUser", "classModel.html#ad2d1b50c1c071ad0b7820cf4343b2463", null ]
];