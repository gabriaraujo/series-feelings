var classCommentView =
[
    [ "CommentView", "classCommentView.html#a07c3539ae4a71691d920119bcff45a36", null ],
    [ "~CommentView", "classCommentView.html#a8c921853ff0393e3320bd1d05d889cb6", null ],
    [ "edit_button_clicked", "classCommentView.html#ad480416c826ca28f0bf9341d2133eb9e", null ],
    [ "getEditComment", "classCommentView.html#a2c4b86211e14004c46b7c460751c2d5e", null ],
    [ "getModel", "classCommentView.html#a5f1a1c0d81d789f5f71a663f9bc549d9", null ],
    [ "getSerie", "classCommentView.html#adaafcb5028a0b90805ce9629ba5fc879", null ],
    [ "inicialize", "classCommentView.html#a4a790fda7648bb59066e0beebd0266e0", null ],
    [ "on_back_button_pressed", "classCommentView.html#a8431a2567caf4b9493b0569b23cd9342", null ],
    [ "setEditComment", "classCommentView.html#adc087f605193259cc81d82d6e9331e52", null ],
    [ "setModel", "classCommentView.html#a75bae66f5e39674c99a8edd729bfb45c", null ],
    [ "setSerie", "classCommentView.html#a492d71ab52535f77c1fa47da3426931e", null ],
    [ "editComment", "classCommentView.html#a0a45beef35d8273454a1a77c376cbb85", null ],
    [ "model", "classCommentView.html#a69a16f8384bbe093c17ee61597f9c29b", null ],
    [ "serie", "classCommentView.html#ad069a5f577ac86c6922a5fddcb6e2db4", null ],
    [ "ui", "classCommentView.html#a1485124218ba355dadff53052f1280f3", null ]
];