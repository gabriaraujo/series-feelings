var classProfileEdit =
[
    [ "ProfileEdit", "classProfileEdit.html#aa9285f182593f03600a57cca594034a4", null ],
    [ "~ProfileEdit", "classProfileEdit.html#ac9ec7aad9137b5fa47337e009946051d", null ],
    [ "getModel", "classProfileEdit.html#a7284dd66c306bfde777391759bf4ab72", null ],
    [ "initialize", "classProfileEdit.html#a8e8a04a4493130bd3058996276b8453b", null ],
    [ "on_delete_account_button_pressed", "classProfileEdit.html#abb084ba0baa447b9bb62fa36091b766d", null ],
    [ "on_pushButton_pressed", "classProfileEdit.html#aaa14318a2b6a741228db775fb7420a9d", null ],
    [ "on_save_button_pressed", "classProfileEdit.html#a0d5dea7854c6832cf0f3b191994711ed", null ],
    [ "setModel", "classProfileEdit.html#aeb9ed68e634c8ee9fbfdba971e51b154", null ],
    [ "model", "classProfileEdit.html#a4bdc051871aa2b9d2e0b94e2df13aee0", null ],
    [ "ui", "classProfileEdit.html#a0dba477db08912456f816d2e073ec0f9", null ]
];