var classSerieImpl =
[
    [ "SerieImpl", "classSerieImpl.html#a2ef25fb73aea469315fde646f4467ec4", null ],
    [ "SerieImpl", "classSerieImpl.html#afacb1674e0dd1a9cf7a852648c281c58", null ],
    [ "SerieImpl", "classSerieImpl.html#ae7fd3a16d37c446bdde2c0600b986a53", null ],
    [ "~SerieImpl", "classSerieImpl.html#ae6926a05224ed02ee6794c3b57c40efb", null ],
    [ "getCommentList", "classSerieImpl.html#a9a02b62aa1c368b3d453214b3324025c", null ],
    [ "getName", "classSerieImpl.html#a02d423bf1ad5fc75d0e899ca6c5c5a2e", null ],
    [ "operator=", "classSerieImpl.html#a2ed9cd874ee350e9f7f1643f417d513b", null ],
    [ "setCommentList", "classSerieImpl.html#a3daa074daf310758b38b8d9330901926", null ],
    [ "setName", "classSerieImpl.html#a23b0bac9a9033d08ba9fde9c400027e9", null ],
    [ "comment_list", "classSerieImpl.html#a15435803b802b15bcebdcc5723e9f08b", null ],
    [ "name", "classSerieImpl.html#a5c841a1aeade20ce3ad432d6e243ecbc", null ]
];