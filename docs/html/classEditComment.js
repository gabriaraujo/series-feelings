var classEditComment =
[
    [ "EditComment", "classEditComment.html#a47261d1b8166fe999389f92673f97d8a", null ],
    [ "~EditComment", "classEditComment.html#a78e7066d2d80ce83ab6d4e25d3e03ab6", null ],
    [ "getComment", "classEditComment.html#a5bf2bbfd820d1c7d4d1c280d74df264e", null ],
    [ "getModel", "classEditComment.html#ab642f56cbdbb6832e6badf46edc8e5bf", null ],
    [ "getSerie", "classEditComment.html#a5504f2c1e8d65a62736e50c9a257528a", null ],
    [ "inicialize", "classEditComment.html#a583bfaae5bea7325bf8d6eb0cb445bd6", null ],
    [ "on_back_button_pressed", "classEditComment.html#a9affdff42440df1a4aba72b0aa7e351b", null ],
    [ "on_cancel_button_pressed", "classEditComment.html#acbba4a69f914715f89af95b43b1c2a94", null ],
    [ "on_save_button_pressed", "classEditComment.html#aa3ccf12e8487ff171e716b440515da66", null ],
    [ "setComment", "classEditComment.html#a58670cef3cfd57f3921566f4dcdf692a", null ],
    [ "setModel", "classEditComment.html#afb2e7aba0380b5f416a61d04bcabd08c", null ],
    [ "setSerie", "classEditComment.html#a43ab5ee0f68c6568e40dc2f52974c5a5", null ],
    [ "c", "classEditComment.html#a6e551cb99b76fc72333da1bccbeedcdb", null ],
    [ "model", "classEditComment.html#a16d561c6c0a8efdbc7e1d248bd24d885", null ],
    [ "serie", "classEditComment.html#a1ac856b466d4c580fd8a97fac97e2b28", null ],
    [ "ui", "classEditComment.html#ac675acbaf6b18b4f8acafd1ae63838e2", null ]
];