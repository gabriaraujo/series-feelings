var annotated_dup =
[
    [ "AddComment", "classAddComment.html", "classAddComment" ],
    [ "Comment", "classComment.html", "classComment" ],
    [ "CommentImpl", "classCommentImpl.html", "classCommentImpl" ],
    [ "CommentView", "classCommentView.html", "classCommentView" ],
    [ "EditComment", "classEditComment.html", "classEditComment" ],
    [ "HomePage", "classHomePage.html", "classHomePage" ],
    [ "Login", "classLogin.html", "classLogin" ],
    [ "Model", "classModel.html", "classModel" ],
    [ "ModelImpl", "classModelImpl.html", "classModelImpl" ],
    [ "ProfileEdit", "classProfileEdit.html", "classProfileEdit" ],
    [ "Register", "classRegister.html", "classRegister" ],
    [ "RegisterImpl", "classRegisterImpl.html", "classRegisterImpl" ],
    [ "RegisterView", "classRegisterView.html", "classRegisterView" ],
    [ "Serie", "classSerie.html", "classSerie" ],
    [ "SerieImpl", "classSerieImpl.html", "classSerieImpl" ],
    [ "SerieView", "classSerieView.html", "classSerieView" ],
    [ "User", "classUser.html", "classUser" ],
    [ "UserImpl", "classUserImpl.html", "classUserImpl" ]
];