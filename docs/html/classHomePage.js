var classHomePage =
[
    [ "HomePage", "classHomePage.html#a6b43d601ca302278ee9424f71841a4aa", null ],
    [ "~HomePage", "classHomePage.html#aff8e741021104752949c6935c7407f90", null ],
    [ "getLogin", "classHomePage.html#a90e3bb900d65adea0434caa429609b63", null ],
    [ "getModel", "classHomePage.html#a727553eefbf8bdcd71905510e85b5559", null ],
    [ "getProfileEdit", "classHomePage.html#aed12eab244e4dab8f7af7a489a76f17a", null ],
    [ "getSeries", "classHomePage.html#a04577776577e5ec15e45df423b406413", null ],
    [ "on_edit_profile_button_pressed", "classHomePage.html#ac7215d58e343742d969a2ccce17f8ab0", null ],
    [ "on_login_button_pressed", "classHomePage.html#a8de463d76be70521037e06764e603f90", null ],
    [ "on_logout_button_pressed", "classHomePage.html#aa6d8fc206901aaa9750673b9816648b8", null ],
    [ "on_shows_button_pressed", "classHomePage.html#ac35acd02b08c5e64a816f4893d0de729", null ],
    [ "setLogin", "classHomePage.html#aa9fcbaa756cb8dfeb2ddb6ece3fff692", null ],
    [ "setModel", "classHomePage.html#a194409bde76ef54a075d99b0d67918dd", null ],
    [ "setProfileEdit", "classHomePage.html#a2ebb3ac8a4d0894d1590487b14191902", null ],
    [ "setSeries", "classHomePage.html#a3cbdd0ff44db711a2e0551b8bd21b575", null ],
    [ "login", "classHomePage.html#a72d2a76710ceda2329e1802a83ff46bd", null ],
    [ "model", "classHomePage.html#a18c8c3d0c1b526c18e287196d3080efc", null ],
    [ "profileEdit", "classHomePage.html#a40e0a3b1913f0c654e0c3f293e823672", null ],
    [ "series", "classHomePage.html#a0237526dd23b6e6557523626bf2363d8", null ],
    [ "ui", "classHomePage.html#ae5f1197f0c3aa11acf90d036ae4f91c7", null ]
];