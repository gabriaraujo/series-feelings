var classUser =
[
    [ "~User", "classUser.html#a7d1cf7c22ba031caec015ec427419513", null ],
    [ "getBirth", "classUser.html#ac4b0a969063e2212401a61400d86b9a6", null ],
    [ "getEmail", "classUser.html#a58eae5ae9bc079ca5ee31c41326b229e", null ],
    [ "getFavorites", "classUser.html#a546911a739cc0f49168656983a7ba29c", null ],
    [ "getGender", "classUser.html#a75e9f2af43220413fa30e9a130f939f2", null ],
    [ "getName", "classUser.html#a10b64aca04c37b66fcb5a112e87f97ac", null ],
    [ "getPassword", "classUser.html#a29d4b884ba9f2a3f28a77368c86239fd", null ],
    [ "getPermission", "classUser.html#ae326f0c51a673f749ed57b9f0d0b723a", null ],
    [ "operator=", "classUser.html#afd7134b455781d42362cf5fd0d5d2e68", null ],
    [ "operator==", "classUser.html#a461783a03e5a694a2cdd80fba7b18459", null ],
    [ "setBirth", "classUser.html#a6585d80877c48a2b2b2d55479891f5ae", null ],
    [ "setEmail", "classUser.html#ac72e220fdcbb3decc392d2729f27e6b5", null ],
    [ "setFavorites", "classUser.html#ae85f12589ea7206a6d75e232452e6e4b", null ],
    [ "setGender", "classUser.html#a37458e03b05b04f5dad1dad700f648e8", null ],
    [ "setName", "classUser.html#a47339f4f166d9baa023fdd59e81c965d", null ],
    [ "setPassword", "classUser.html#a809c17ec3427917ae2e641d171cb99d2", null ],
    [ "setPermission", "classUser.html#ab6a6689e3581c70af166b2eb537df6fb", null ]
];