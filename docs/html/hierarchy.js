var hierarchy =
[
    [ "Comment", "classComment.html", [
      [ "CommentImpl", "classCommentImpl.html", null ]
    ] ],
    [ "Model", "classModel.html", [
      [ "ModelImpl", "classModelImpl.html", null ]
    ] ],
    [ "QDialog", null, [
      [ "AddComment", "classAddComment.html", null ],
      [ "CommentView", "classCommentView.html", null ],
      [ "EditComment", "classEditComment.html", null ],
      [ "HomePage", "classHomePage.html", null ],
      [ "Login", "classLogin.html", null ],
      [ "ProfileEdit", "classProfileEdit.html", null ],
      [ "RegisterView", "classRegisterView.html", null ],
      [ "SerieView", "classSerieView.html", null ]
    ] ],
    [ "Register", "classRegister.html", [
      [ "RegisterImpl", "classRegisterImpl.html", null ]
    ] ],
    [ "Serie", "classSerie.html", [
      [ "SerieImpl", "classSerieImpl.html", null ]
    ] ],
    [ "User", "classUser.html", [
      [ "UserImpl", "classUserImpl.html", null ]
    ] ]
];