var searchData=
[
  ['readcomments_266',['readComments',['../classModel.html#a500a81d76197bf1a0c6a088a34ffa3d6',1,'Model::readComments()'],['../classModelImpl.html#af996e3a1067cb3383c53aec7337af68a',1,'ModelImpl::readComments()']]],
  ['readseries_267',['readSeries',['../classModel.html#a814142f468ebca45b0ef14b6b922397a',1,'Model::readSeries()'],['../classModelImpl.html#a6d14c556105d6d998a27098270fecc48',1,'ModelImpl::readSeries()']]],
  ['readuser_268',['readUser',['../classModel.html#ad3fa273a7dc1397ce748c657e552d6cc',1,'Model::readUser()'],['../classModelImpl.html#a132c46a2d6ca86c7374a065b3079fe15',1,'ModelImpl::readUser()']]],
  ['registerimpl_269',['RegisterImpl',['../classRegisterImpl.html#a601b77faf4f07f51170821a189fed94f',1,'RegisterImpl']]],
  ['registerview_270',['RegisterView',['../classRegisterView.html#a731451080c7f6da600a1fecc416fdeca',1,'RegisterView']]],
  ['remove_271',['remove',['../classRegister.html#a09d7d6c24f7c07e97f7ef2d24a057f73',1,'Register::remove(Model *, User *)=0'],['../classRegister.html#a1f54d2a2fb3fe0e42edba13d0ac00071',1,'Register::remove(Model *, Serie *, User *, Comment *)=0'],['../classRegisterImpl.html#a528ea874f5607abfea9fea4b8cab1628',1,'RegisterImpl::remove(Model *, User *)'],['../classRegisterImpl.html#a526915371b3ca3505a0b14168b4286ee',1,'RegisterImpl::remove(Model *, Serie *, User *, Comment *)']]]
];
