var classSerieView =
[
    [ "SerieView", "classSerieView.html#afdb8a42c3a0c1524675fd3b7d43f235f", null ],
    [ "~SerieView", "classSerieView.html#aa0895f177c171e8eb1e9bc10b4ac0d92", null ],
    [ "comment_button_pressed", "classSerieView.html#ace8d7cb49f653e9634de4f5d75624763", null ],
    [ "getAddComment", "classSerieView.html#a982f1c5856253c5870b61a3f589fe543", null ],
    [ "getComments", "classSerieView.html#a39d1172ffd1f3f3f4e3d8ccc45ececba", null ],
    [ "getModel", "classSerieView.html#a754eab94fdb5e9c72131dc96a8723281", null ],
    [ "on_back_button_pressed", "classSerieView.html#a5a733a6db82fab72918c0b8e0265460a", null ],
    [ "see_comment_button_pressed", "classSerieView.html#aa357f36a107278886cbd41c04bfe1ff4", null ],
    [ "setAddComment", "classSerieView.html#af798d785ab462c4d41ef4bba25ad3877", null ],
    [ "setComments", "classSerieView.html#aee848b427f924b88d84fd4186a528983", null ],
    [ "setModel", "classSerieView.html#add4cfa40f528c3bb81ae815dd71f5359", null ],
    [ "setTableData", "classSerieView.html#ac9d3a68d878e899d9e1031f53bd24ee1", null ],
    [ "addComment", "classSerieView.html#afd6f7bfb5e59447b613a1cb7f3551c05", null ],
    [ "comments", "classSerieView.html#a4789a2c6581c6e0788a20452ce28a86b", null ],
    [ "model", "classSerieView.html#a8c2194db4d31060bd7d49302479f2258", null ],
    [ "ui", "classSerieView.html#a935cd7f615d059e63b63a727ec93d218", null ]
];