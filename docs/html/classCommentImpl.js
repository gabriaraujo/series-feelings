var classCommentImpl =
[
    [ "CommentImpl", "classCommentImpl.html#ac00ffc212a10982ff6468bb65f75ddd4", null ],
    [ "CommentImpl", "classCommentImpl.html#a684422bb7c146ed8fe629df9fa0abee9", null ],
    [ "CommentImpl", "classCommentImpl.html#af172575674ed2b3d80599b88d7b65426", null ],
    [ "~CommentImpl", "classCommentImpl.html#a8013a41a06183d7eb68220ab2cd3cc70", null ],
    [ "getComment", "classCommentImpl.html#ae4d278b1b60aed181a5dbf42cb34ab71", null ],
    [ "getUser", "classCommentImpl.html#ac491d35e3be11ca971528aff3f1daaad", null ],
    [ "operator=", "classCommentImpl.html#afcd9c6f59c2d5dda8722c305c312e881", null ],
    [ "setComment", "classCommentImpl.html#ad1bebaab05872a9fe8394ebd4b719df7", null ],
    [ "setUser", "classCommentImpl.html#acccdd5e8413a3cfa30417b3707da8576", null ],
    [ "comment", "classCommentImpl.html#a2611e789c78066de1b79eeac6bdd6b9a", null ],
    [ "user", "classCommentImpl.html#af4fbfbe87a20bfed0079f9a5b65a4753", null ]
];