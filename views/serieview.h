#ifndef SERIEVIEW_H
#define SERIEVIEW_H

#include "commentview.h"
#include "addcomment.h"

namespace Ui {
class SerieView;
}

/**
 * @brief This class represents a SerieView UI
 * 
 */
class SerieView : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Serie View object
     * 
     * @param parent 
     */
    explicit SerieView(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Serie View object
     * 
     */
    ~SerieView();
    
    /**
     * @brief Set the Table Data object
     * 
     */
    void setTableData();

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model *getModel() const;

    /**
     * @brief Set the Model object
     * 
     * @param value the model pointer
     */
    void setModel(Model *value);

    /**
     * @brief Get the Comments object
     * 
     * @return CommentView* the CommentView pointer
     */
    CommentView *getComments() const;

    /**
     * @brief Set the Comments object
     * 
     * @param value the CommentView pointer
     */
    void setComments(CommentView *value);

    /**
     * @brief Get the Add Comment object
     * 
     * @return AddComment* the AddComment pointer
     */
    AddComment *getAddComment() const;

    /**
     * @brief Set the Add Comment object
     * 
     * @param value the AddComment pointer
     */
    void setAddComment(AddComment *value);

private slots:
    /**
     * @brief the button function
     * 
     */
    void on_back_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void comment_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void see_comment_button_pressed();

private:
    Ui::SerieView *ui;
    Model* model;
    CommentView* comments;
    AddComment* addComment;
};

#endif // SERIEVIEW_H
