#include "addcomment.h"
#include "ui_addcomment.h"

AddComment::AddComment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddComment)
{
    ui->setupUi(this);
    this->setWindowTitle("Adicionar comentário");
}

AddComment::~AddComment()
{
    delete ui;
}

Serie *AddComment::getSerie() const
{
    return serie;
}

void AddComment::setSerie(Serie *value)
{
    serie = value;
    ui->comment->clear();
    this->inicialize();
}

Model *AddComment::getModel() const
{
    return model;
}

void AddComment::setModel(Model *value)
{
    model = value;
}

void AddComment::on_cancel_button_pressed()
{
    this->close();
}

void AddComment::on_back_button_pressed()
{
    this->close();
}

void AddComment::inicialize(){
    ui->serie_name->setText(QString::fromStdString(serie->getName()));
}

void AddComment::on_save_button_pressed()
{
    string comment_text = ui->comment->toPlainText().toStdString();
    if(comment_text.empty()){
        QMessageBox::information(
            this, tr("Comentário"),
            tr("Comentário vazio!")
        );

        return;
    }

    Comment* comment = this->model->createComment(this->model->getUser(), comment_text);
    this->serie->getCommentList().push_back(comment);
    this->model->writeComments("../series-feelings/database/comments.csv");
    this->close();
}
