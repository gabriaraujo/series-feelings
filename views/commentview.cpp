#include "commentview.h"
#include "ui_commentview.h"

#include <iostream>
using namespace std;

CommentView::CommentView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CommentView)
{
    ui->setupUi(this);
    this->setWindowTitle("Comentários");
}

CommentView::~CommentView()
{
    delete ui;
}

void CommentView::inicialize() {
    ui->name_label->setText(QString::fromStdString(serie->getName()));

    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setRowCount((int)serie->getCommentList().size());

    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->horizontalHeader()->hide();
    ui->tableWidget->setShowGrid(false);
    ui->tableWidget->setColumnWidth(0, 100);
    ui->tableWidget->setColumnWidth(1, 500);
    ui->tableWidget->setColumnWidth(3, 120);
    ui->tableWidget->setSortingEnabled(false);

    int count = 0;
    for(auto it : serie->getCommentList()){
        QString name = QString::fromStdString(it->getUser()->getName());
        QTableWidgetItem *name_item = new QTableWidgetItem(name, Qt::DisplayRole);
        name_item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        name_item->setTextAlignment(Qt::AlignCenter);

        QString comment = QString::fromStdString(it->getComment());
        QTableWidgetItem *c_item = new QTableWidgetItem(comment, Qt::DisplayRole);
        c_item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        c_item->setTextAlignment(Qt::AlignCenter);

        QPushButton* edit_button = new QPushButton("Editar");
        edit_button->setStyleSheet("QPushButton {color: #43BCFE;}");

        if (
            model->getUser() != nullptr
            && it->getUser()->getEmail() == model->getUser()->getEmail()
        ) {
            ui->tableWidget->setCellWidget(count, 2, edit_button);

        } else {
            QTableWidgetItem *item = new QTableWidgetItem();
            item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
            ui->tableWidget->setItem(count, 2, item);
        }

        ui->tableWidget->setItem(count, 0, name_item);
        ui->tableWidget->setItem(count, 1, c_item);

        connect(edit_button, SIGNAL(clicked()), this, SLOT(edit_button_clicked()));

        count++;
    }
    ui->tableWidget->setSortingEnabled(true);
}

void CommentView::on_back_button_pressed()
{
    this->close();
}

void CommentView::edit_button_clicked()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    int row = ui->tableWidget->indexAt(buttonSender->pos()).row();

    for (auto it : serie->getCommentList())
        if (it->getComment() == ui->tableWidget->item(row, 1)->text().toStdString()) {
            editComment->setSerie(serie);
            editComment->setComment(it);
            break;
        }

    editComment->inicialize();
    editComment->exec();
    this->close();
}

EditComment *CommentView::getEditComment() const
{
    return editComment;
}

void CommentView::setEditComment(EditComment *value)
{
    editComment = value;
}

Model *CommentView::getModel() const
{
    return model;
}

void CommentView::setModel(Model *value)
{
    model = value;
}

Serie *CommentView::getSerie() const
{
    return serie;
}

void CommentView::setSerie(Serie *value)
{    
    serie = value;
    ui->tableWidget->clear();
    this->inicialize();
}
