#include "editcomment.h"
#include "ui_editcomment.h"

EditComment::EditComment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::EditComment)
{
    ui->setupUi(this);
}

EditComment::~EditComment()
{
    delete ui;
}

Comment *EditComment::getComment() const
{
    return c;
}

void EditComment::setComment(Comment *value)
{
    c = value;
}

Serie *EditComment::getSerie() const
{
    return serie;
}

void EditComment::setSerie(Serie *value)
{
    serie = value;
}

Model *EditComment::getModel() const
{
    return model;
}

void EditComment::setModel(Model *value)
{
    model = value;
}

void EditComment::inicialize() {
    ui->comment->clear();
    ui->serie_name->setText(QString::fromStdString(serie->getName()));
    ui->comment->setText(QString::fromStdString(c->getComment()));
}

void EditComment::on_save_button_pressed()
{
    c->setComment(ui->comment->toPlainText().toStdString());
    model->writeComments("../series-feelings/database/comments.csv");
    this->close();
}

void EditComment::on_cancel_button_pressed()
{
    Register* reg = Register::createRegister();
    reg->remove(model, serie, model->getUser(), c);
    this->close();
}

void EditComment::on_back_button_pressed()
{
    this->close();
}
