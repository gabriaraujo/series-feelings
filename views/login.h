#ifndef LOGIN_H
#define LOGIN_H

#include "registerview.h"

namespace Ui {
class Login;
}

/**
 * @brief this class representer a Login UI
 * 
 */
class Login : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Login object
     * 
     * @param parent 
     */
    explicit Login(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Login object
     * 
     */
    ~Login();

    /**
     * @brief Get the Reg object
     * 
     * @return RegisterView* the registerView pointer
     */
    RegisterView *getReg() const;

    /**
     * @brief Set the Reg object
     * 
     * @param value the registerView pointer
     */
    void setReg(RegisterView *value);

    Model *getModel() const;
    void setModel(Model *value);

private slots:
    /**
     * @brief the function button
     * 
     */
    void on_register_button_pressed();

    /**
     * @brief the function button
     * 
     */
    void on_back_button_pressed();

    /**
     * @brief the function button
     */
    void on_login_button_pressed();

private:
    Ui::Login *ui;
    Model* model;
    RegisterView* reg;
};

#endif // LOGIN_H
