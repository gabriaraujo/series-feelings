#ifndef PROFILEEDIT_H
#define PROFILEEDIT_H

#include <QDialog>
#include <QMessageBox>
#include "../src/api/register.h"

namespace Ui {
class ProfileEdit;
}

/**
 * @brief this class representer a ProfileEdit UI
 * 
 */
class ProfileEdit : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Profile Edit object
     * 
     * @param parent 
     */
    explicit ProfileEdit(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Profile Edit object
     * 
     */
    ~ProfileEdit();

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model *getModel() const;

    /**
     * @brief Set the Model object
     * 
     * @param value the model pointer
     */
    void setModel(Model *value);

    /**
     * @brief inicilize the ui
     * 
     */
    void initialize();

private slots:
    /**
     * @brief the button function
     * 
     */
    void on_save_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void on_delete_account_button_pressed();

    /**
     * @brief the button fuction
     */
    void on_pushButton_pressed();

private:
    Ui::ProfileEdit *ui;
    Model* model;
};

#endif // PROFILEEDIT_H
