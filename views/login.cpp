#include "login.h"
#include "ui_login.h"

Login::Login(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    this->setWindowTitle("Login");
}

Login::~Login()
{
    delete ui;
}

RegisterView *Login::getReg() const
{
    return reg;
}

void Login::setReg(RegisterView *value)
{
    reg = value;
}

Model *Login::getModel() const
{
    return model;
}

void Login::setModel(Model *value)
{
    model = value;
}

void Login::on_register_button_pressed()
{
    this->close();
    reg->exec();
}

void Login::on_back_button_pressed()
{
    this->close();
}

void Login::on_login_button_pressed() {
    Register* r = Register::createRegister();
    if (!r->search(this->model, ui->username->text().toStdString())) {
        QMessageBox::information(
            this,
            tr("Login"),
            tr("Usuário não encontrado!")
        );

        return;
    }

    User* user = r->consult(this->model, ui->username->text().toStdString());
    if (user->getPassword() != ui->password->text().toStdString()) {
        QMessageBox::information(
            this,
            tr("Login"),
            tr("Senha incorreta!")
        );

        return;
    }

    model->setUser(user);
    this->close();
}
