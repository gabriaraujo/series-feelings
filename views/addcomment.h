#ifndef ADDCOMMENT_H
#define ADDCOMMENT_H

#include <QDialog>
#include <QMessageBox>
#include "../src/api/model.h"

namespace Ui {
class AddComment;
}

/**
 * @brief This class representes a AddComment UI
 * 
 */
class AddComment : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Add Comment object
     * 
     * @param parent 
     */
    explicit AddComment(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Add Comment object
     * 
     */
    ~AddComment();

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model *getModel() const;

    /**
     * @brief Set the Model object
     * 
     * @param value the model pointer
     */
    void setModel(Model *value);

    /**
     * @brief Get the Serie object
     * 
     * @return Serie* the serie pointer
     */
    Serie *getSerie() const;

    /**
     * @brief Set the Serie object
     * 
     * @param value the serie pointer
     */
    void setSerie(Serie *value);

    /**
     * @brief inicialize the UI
     * 
     */
    void inicialize();

private slots:
    /**
     * @brief the button function
     * 
     */
    void on_back_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void on_cancel_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void on_save_button_pressed();

private:
    Ui::AddComment *ui;
    Model* model;
    Serie* serie;
};

#endif // ADDCOMMENT_H
