#ifndef EDITCOMMENT_H
#define EDITCOMMENT_H

#include <QDialog>
#include "src/api/register.h"

namespace Ui {
class EditComment;
}
/**
 * @brief This class represents an EditComent UI
 * 
 */
class EditComment : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Edit Comment object
     * 
     * @param parent 
     */
    explicit EditComment(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Edit Comment object
     * 
     */
    ~EditComment();

    /**
     * @brief Get the Comment object
     * 
     * @return Comment* the comment pointer
     */
    Comment *getComment() const;

    /**
     * @brief Set the Comment object
     * 
     * @param value the comment ui
     */
    void setComment(Comment *value);

    /**
     * @brief Get the Serie object
     * 
     * @return Serie* the serie pointer
     */
    Serie *getSerie() const;

    /**
     * @brief Set the Serie object
     * 
     * @param value the serie pointer
     */
    void setSerie(Serie *value);

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model *getModel() const;

    /**
     * @brief Set the Model object
     * 
     * @param value the model pointer
     */
    void setModel(Model *value);

    /**
     * @brief inicialize the UI
     * 
     */
    void inicialize();

private slots:
    /**
     * @brief the button function
     * 
     */
    void on_save_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void on_cancel_button_pressed();

    void on_back_button_pressed();

private:
    Ui::EditComment *ui;
    Model* model;
    Serie* serie;
    Comment* c;
};

#endif // EDITCOMMENT_H
