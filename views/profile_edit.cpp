#include "profile_edit.h"
#include "ui_profile_edit.h"

ProfileEdit::ProfileEdit( QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProfileEdit)
{
    ui->setupUi(this);
    this->setWindowTitle("Editar Perfil");
}

ProfileEdit::~ProfileEdit()
{
    delete ui;
}

Model *ProfileEdit::getModel() const
{
    return model;
}

void ProfileEdit::setModel(Model *value)
{
    model = value;
}

void ProfileEdit::initialize(){
    ui->full_name_edit->setText(QString::fromStdString(model->getUser()->getName()));
    ui->email_edit->setText(QString::fromStdString(model->getUser()->getEmail()));
    QString date = QString::fromStdString(model->getUser()->getBirth());
    ui->birth_edit->setDate(QDate::fromString(date, "dd/MM/yyyy"));

    if(model->getUser()->getGender() == 'F')
        ui->radioButton->setChecked(true);
    else if(model->getUser()->getGender() == 'M')
        ui->radioButton_2->setChecked(true);
    else
        ui->radioButton_3->setChecked(true);


    QList<QCheckBox*> all_cb = ui->groupBox->findChildren<QCheckBox*>();
    for(int i = 0; i < all_cb.size(); i++){
        if(model->getUser()->getFavorites().at(i) == true)
            all_cb.at(i)->setChecked(true);
        else
            all_cb.at(i)->setChecked(false);
    }
}

void ProfileEdit::on_save_button_pressed()
{
    char gender;
    vector<bool> favorites(6, false);

    if (ui->radioButton->isChecked())
        gender = ui->radioButton->text().toStdString()[0];

    else if (ui->radioButton_2->isChecked())
        gender = ui->radioButton_2->text().toStdString()[0];

    else
        gender = ui->radioButton_3->text().toStdString()[0];

    QList<QCheckBox*> all_cb = ui->groupBox->findChildren<QCheckBox*>();
    for(int i = 0; i < all_cb.size(); i++)
        if(all_cb.at(i)->isChecked())
            favorites.at(i) = true;

    Register* reg = Register::createRegister();

    while (ui->full_name_edit->text().toStdString() == ""
        || ui->email_edit->text().toStdString() == ""
        || ui->birth_edit->text().toStdString() == ""
        || ui->password_edit->text().toStdString() == ""
        || ui->new_password_edit->text().toStdString() == "") {

        QMessageBox::information(
            this, tr("Editar Perfil"),
            tr("Preencha todos os campos!")
        );
        return;
    }

    if (!reg->ValidateEmail(ui->email_edit->text().toStdString())) {
        QMessageBox::information(
            this, tr("Editar Perfil"),
            tr("E-mail inválido!")
        );

        return;
    }

    if (ui->password_edit->text().toStdString() != model->getUser()->getPassword()) {
        QMessageBox::information(
            this, tr("Editar Perfil"),
            tr("Senha Incorreta!")
        );

        return;
    }

    reg->update(
        this->model,
        model->getUser(),
        ui->full_name_edit->text().toStdString(),
        ui->email_edit->text().toStdString(),
        ui->birth_edit->text().toStdString(),
        gender,
        ui->new_password_edit->text().toStdString(),
        favorites
    );

    delete reg;

    QMessageBox::information(this, tr("Editar Perfil"), tr("Perfil Atualizado!"));
    this->close();
}

void ProfileEdit::on_delete_account_button_pressed()
{
    Register* reg = Register::createRegister();
    reg->remove(this->model, this->model->getUser());
    QMessageBox::information(this, tr("Editar Perfil"), tr("Perfil Deletado!"));
    this->close();
}

void ProfileEdit::on_pushButton_pressed()
{
    this->close();
}
