#ifndef HOMEPAGE_H
#define HOMEPAGE_H

#include "login.h"
#include "profile_edit.h"
#include "serieview.h"

namespace Ui {
class HomePage;
}

/**
 * @brief This class represents a HomePage UI
 * 
 */
class HomePage : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Home Page object
     * 
     * @param parent 
     */
    explicit HomePage(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Home Page object
     * 
     */
    ~HomePage();

    /**
     * @brief Get the Login object
     * 
     * @return Login* the login pointer
     */
    Login *getLogin() const;

    /**
     * @brief Set the Login object
     * 
     * @param value the login pointer
     */
    void setLogin(Login *value);

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model *getModel() const;

    /**
     * @brief Set the Model object
     * 
     * @param value the model pointer
     */
    void setModel(Model *value);

    /**
     * @brief Get the Profile Edit object
     * 
     * @return ProfileEdit* the profileEdit pointer
     */
    ProfileEdit *getProfileEdit() const;

    /**
     * @brief Set the Profile Edit object
     * 
     * @param value the profileEdit pointer
     */
    void setProfileEdit(ProfileEdit *value);

    SerieView *getSeries() const;

    void setSeries(SerieView *value);

private slots:
    /**
     * @brief the button funciton
     *
     */
    void on_login_button_pressed();

    /**
     * @brief the buton function
     * 
     */
    void on_edit_profile_button_pressed();

    /**
     * @brief the button function
     */
    void on_logout_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void on_shows_button_pressed();

private:
    Ui::HomePage *ui;
    Model* model;
    Login* login;
    ProfileEdit* profileEdit;
    SerieView* series;
};

#endif // HOMEPAGE_H
