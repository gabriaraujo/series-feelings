#include "homepage.h"
#include "ui_homepage.h"
#include <iostream>

HomePage::HomePage(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HomePage)
{
    ui->setupUi(this);
    this->setWindowTitle("Home");
    ui->logout_button->setVisible(false);
}

HomePage::~HomePage()
{
    delete ui;
}

Login *HomePage::getLogin() const
{
    return login;
}

void HomePage::setLogin(Login *value)
{
    login = value;
}

void HomePage::on_login_button_pressed()
{
    login->exec();
    if(model->getUser() != nullptr){
        ui->login_button->setVisible(false);
        ui->logout_button->setVisible(true);
    }
}

void HomePage::on_edit_profile_button_pressed()
{
    if(this->model->getUser() == nullptr)
        QMessageBox::information(
            this, tr("Erro"),
            tr("Você precisa fazer o login primeiro!")
        );
    else{
        profileEdit->initialize();
        profileEdit->exec();
    }
}

ProfileEdit *HomePage::getProfileEdit() const
{
    return profileEdit;
}

void HomePage::setProfileEdit(ProfileEdit *value)
{
    profileEdit = value;
}

Model *HomePage::getModel() const
{
    return model;
}

void HomePage::setModel(Model *value)
{
    model = value;
}

void HomePage::on_logout_button_pressed()
{
    model->setUser(nullptr);
    QMessageBox::information(
        this, tr("Logout"),
        tr("Você foi deslogado do sistema!")
    );
    ui->login_button->setVisible(true);
    ui->logout_button->setVisible(false);
}

void HomePage::on_shows_button_pressed()
{
    this->series->setTableData();
    this->series->exec();
}

SerieView *HomePage::getSeries() const
{
    return series;
}

void HomePage::setSeries(SerieView *value)
{
    series = value;
}
