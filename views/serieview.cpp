#include "serieview.h"
#include "ui_serieview.h"
#include <QDebug>
#include <iostream>
using namespace std;

SerieView::SerieView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SerieView)
{
    ui->setupUi(this);
    this->setWindowTitle("Catálogo de Series");
}

SerieView::~SerieView()
{
    delete ui;
}

void SerieView::on_back_button_pressed()
{
    ui->tableWidget->clearContents();
    this->close();
}

CommentView *SerieView::getComments() const
{
    return comments;
}

void SerieView::setComments(CommentView *value)
{
    comments = value;
}

Model *SerieView::getModel() const
{
    return model;
}

void SerieView::setModel(Model *value)
{
    model = value;
}

void SerieView::setTableData(){
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setRowCount((int)model->getSerieList().size());
    ui->tableWidget->verticalHeader()->hide();
    ui->tableWidget->horizontalHeader()->hide();
    ui->tableWidget->setShowGrid(false);
    ui->tableWidget->setColumnWidth(0, 230);
    ui->tableWidget->setColumnWidth(1, 250);
    ui->tableWidget->setColumnWidth(3, 150);

    int count = 0;
    ui->tableWidget->setSortingEnabled(false);
    for(auto it : this->model->getSerieList()){
        QString name = QString::fromStdString(it->getName());
        QTableWidgetItem *name_item = new QTableWidgetItem(name, Qt::DisplayRole);
        name_item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);

        QString size = QString::number(it->getCommentList().size());
        QTableWidgetItem *size_item = new QTableWidgetItem(size, Qt::DisplayRole);
        size_item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
        size_item->setTextAlignment(Qt::AlignCenter);

        ui->tableWidget->setItem(count, 0, name_item);
        ui->tableWidget->setItem(count, 1, size_item);

        if(this->model->getUser() != nullptr){
            ui->tableWidget->showColumn(2);
            QPushButton* comment = new QPushButton("Comentar");
            comment->setStyleSheet("QPushButton {color: #43BCFE;}");
            ui->tableWidget->setCellWidget(count, 2, comment);
            connect(comment, SIGNAL(pressed()), this, SLOT(comment_button_pressed()));
        }
        else{
            ui->tableWidget->hideColumn(2);
        }

        QPushButton* see_comments = new QPushButton("Ver Comentários");
        see_comments->setStyleSheet("QPushButton {color: #43BCFE;}");
        ui->tableWidget->setCellWidget(count, 3, see_comments);
        connect(see_comments, SIGNAL(pressed()), this, SLOT(see_comment_button_pressed()));

        count++;
    }
    ui->tableWidget->setSortingEnabled(true);

}

void SerieView::comment_button_pressed(){
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    int row = ui->tableWidget->indexAt(buttonSender->pos()).row();

    for(auto it : this->model->getSerieList())
        if(it->getName() == ui->tableWidget->item(row, 0)->text().toStdString()){
            this->addComment->setSerie(it);
            this->addComment->exec();
            this->close();
        }
}

void SerieView::see_comment_button_pressed(){
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    int row = ui->tableWidget->indexAt(buttonSender->pos()).row();

    for(auto it : this->model->getSerieList())
        if(it->getName() == ui->tableWidget->item(row, 0)->text().toStdString()){
            this->comments->setSerie(it);
            this->comments->exec();
            this->close();
        }
}

AddComment *SerieView::getAddComment() const
{
    return addComment;
}

void SerieView::setAddComment(AddComment *value)
{
    addComment = value;
}
