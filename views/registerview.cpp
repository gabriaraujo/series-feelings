#include "registerview.h"
#include "ui_registerview.h"
#include <QHBoxLayout>

RegisterView::RegisterView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegisterView)
{
    ui->setupUi(this);
    this->setWindowTitle("Cadastro");
}

RegisterView::~RegisterView() {
    delete ui;
}

Model *RegisterView::getModel() const {
    return model;
}

void RegisterView::setModel(Model *value) {
    model = value;
}

void RegisterView::on_push_button_pressed() {
    char gender;
    vector<bool> favorites(6, false);

    if (ui->radio_button->isChecked())
        gender = ui->radio_button->text().toStdString()[0];

    else if (ui->radio_button_2->isChecked())
        gender = ui->radio_button_2->text().toStdString()[0];

    else
        gender = ui->radio_button_3->text().toStdString()[0];

    QList<QCheckBox*> all_cb = ui->group_box->findChildren<QCheckBox*>();
    for(int i = 0; i < all_cb.size(); i++)
        if(all_cb.at(i)->isChecked())
            favorites.at(i) = true;

    Register* reg = Register::createRegister();

    while (ui->name_text->text().toStdString() == ""
        || ui->email_text->text().toStdString() == ""
        || ui->birth_text->text().toStdString() == ""
        || ui->pw_text->text().toStdString() == "") {

        QMessageBox::information(
            this, tr("Cadastro"),
            tr("Preencha todos os campos!")
        );
        return;
    }

    if (!reg->ValidateEmail(ui->email_text->text().toStdString())) {
        QMessageBox::information(
            this, tr("Cadastro"),
            tr("E-mail inválido!")
        );

        return;
    }

    if(reg->search(model, ui->email_text->text().toStdString())){
        QMessageBox::information(
            this, tr("Cadastro"),
            tr("E-mail já cadastrado!")
        );

        return;
    }

    reg->create(
        model,
        ui->name_text->text().toStdString(),
        ui->email_text->text().toStdString(),
        ui->birth_text->text().toStdString(),
        gender,
        ui->pw_text->text().toStdString(),
        favorites,
        0
    );

    delete reg;

    QMessageBox::information(this, tr("Cadastro"), tr("Cadastro Realizado!"));
    this->close();
}

void RegisterView::on_back_button_pressed() {
    this->close();
}
