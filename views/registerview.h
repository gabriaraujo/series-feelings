#ifndef REGISTERVIEW_H
#define REGISTERVIEW_H

#include "src/api/register.h"
#include <QDialog>
#include <QMessageBox>

namespace Ui {
class RegisterView;
}

/**
 * @brief this class representer a RegisterView UI
 * 
 */
class RegisterView : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Register View object
     * 
     * @param parent 
     */
    explicit RegisterView(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Register View object
     * 
     */
    ~RegisterView();

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model* getModel() const;

    /**
     * @brief Set the Model object
     * 
     */
    void setModel(Model*);

private slots:
    /**
     * @brief the button function
     * 
     */
    void on_push_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void on_back_button_pressed();

private:
    Ui::RegisterView *ui;
    Model* model;
};

#endif // REGISTERVIEW_H
