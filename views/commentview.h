#ifndef COMMENTVIEW_H
#define COMMENTVIEW_H

#include <QTableWidget>
#include "editcomment.h"

namespace Ui {
class CommentView;
}

/**
 * @brief This class representes a CommentView UI
 * 
 */
class CommentView : public QDialog
{
    Q_OBJECT

public:
    /**
     * @brief Construct a new Comment View object
     * 
     * @param parent 
     */
    explicit CommentView(QWidget *parent = nullptr);

    /**
     * @brief Destroy the Comment View object
     * 
     */
    ~CommentView();

    /**
     * @brief Inicialize the UI
     * 
     */
    void inicialize();

    /**
     * @brief Get the Serie object
     * 
     * @return Serie* the serie pointer
     */
    Serie *getSerie() const;

    /**
     * @brief Set the Serie object
     * 
     * @param value the serie pointer
     */
    void setSerie(Serie *value);

    /**
     * @brief Get the Model object
     * 
     * @return Model* the model pointer
     */
    Model *getModel() const;

    /**
     * @brief Set the Model object
     * 
     * @param value the model pointer
     */
    void setModel(Model *value);

    /**
     * @brief Get the Edit Comment object
     * 
     * @return EditComment* the EditComment pointer
     */
    EditComment *getEditComment() const;

    /**
     * @brief Set the Edit Comment object
     * 
     * @param value the EditComment pointer
     */
    void setEditComment(EditComment *value);

private slots:
    /**
     * @brief the button function
     * 
     */
    void on_back_button_pressed();

    /**
     * @brief the button function
     * 
     */
    void edit_button_clicked();

private:
    Ui::CommentView *ui;
    EditComment* editComment;
    Model* model;
    Serie* serie;
};

#endif // COMMENTVIEW_H
