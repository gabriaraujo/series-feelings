QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/api/model_impl.cpp \
    src/api/register_impl.cpp \
    src/classes/comment_impl.cpp \
    src/classes/serie_impl.cpp \
    src/classes/user_impl.cpp \
    src/main.cpp \
    views/addcomment.cpp \
    views/commentview.cpp \
    views/editcomment.cpp \
    views/homepage.cpp \
    views/login.cpp \
    views/profile_edit.cpp \
    views/registerview.cpp \
    views/serieview.cpp

HEADERS += \
    src/api/model.h \
    src/api/model_impl.h \
    src/api/register.h \
    src/api/register_impl.h \
    src/classes/comment.h \
    src/classes/comment_impl.h \
    src/classes/serie.h \
    src/classes/serie_impl.h \
    src/classes/user.h \
    src/classes/user_impl.h \
    views/addcomment.h \
    views/commentview.h \
    views/editcomment.h \
    views/homepage.h \
    views/login.h \
    views/profile_edit.h \
    views/registerview.h \
    views/serieview.h

FORMS += \
    views/addcomment.ui \
    views/commentview.ui \
    views/editcomment.ui \
    views/homepage.ui \
    views/login.ui \
    views/registerview.ui \
    views/profile_edit.ui \
    views/serieview.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
    img/button.png

RESOURCES += \
    resource_file.qrc
