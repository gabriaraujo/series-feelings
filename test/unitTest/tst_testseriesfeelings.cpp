#include <QtTest>
#include "../../../series-feelings/src/classes/user_impl.cpp"
#include "../../../series-feelings/src/classes/user.h"
#include "../../../series-feelings/src/classes/comment_impl.cpp"
#include "../../../series-feelings/src/classes/comment.h"
#include "../../../series-feelings/src/classes/serie_impl.cpp"
#include "../../../series-feelings/src/classes/serie.h"
#include "../../../series-feelings/src/api/register_impl.cpp"
#include "../../../series-feelings/src/api/register.h"
#include "../../../series-feelings/src/api/model_impl.cpp"
#include "../../../series-feelings/src/api/model.h"
#include <vector>

class testSeriesFeelings : public QObject
{
    Q_OBJECT

public:
    testSeriesFeelings();
    ~testSeriesFeelings();

private slots:
    void unit_user_constructor();
    void unit_user_operator();
    void unit_user_copyTest();
    void unit_user_assignTest();
    void unit_user_comparison();
    void unit_comment_constructor();
    void unit_comment_operator();
    void unit_comment_copyTest();
    void unit_comment_assignTest();
    void unit_serie_constructor();
    void unit_serie_operator();
    void unit_serie_copyTest();
    void unit_serie_assignTest();
    void unit_register_constructor();
    void unit_register_assignTest();
    void unit_model_constructor();
    void unit_model_assignTest();
};

testSeriesFeelings::testSeriesFeelings(){}

testSeriesFeelings::~testSeriesFeelings(){}

void testSeriesFeelings::unit_user_constructor()
{
    User* user1 = new UserImpl();
    QVERIFY(user1->getName() == "");
    QVERIFY(user1->getEmail() == "");
    QVERIFY(user1->getBirth() == "");
    QVERIFY(user1->getGender() == NULL);
    QVERIFY(user1->getPassword() == "");
    QVERIFY(user1->getFavorites().empty() == true);
    delete user1;
}

void testSeriesFeelings::unit_user_operator()
{
    User *s1 = new UserImpl();
    s1->setName("Richard");
    User* s2 = new UserImpl();
    (*s2) = (*s1);
    QVERIFY(s1 != s2 && s1->getName() == s2->getName());
    delete s1; delete s2;
}

void testSeriesFeelings::unit_user_copyTest()
{
    User *s1 = new UserImpl();
    s1->setName("Richard");
    User* s2 = new UserImpl(s1);
    QVERIFY(s1 != s2 && s1->getName() == s2->getName());
    delete s1; delete s2;
}

void testSeriesFeelings::unit_user_assignTest()
{
    User *s1 = new UserImpl();
    vector<bool> v;
    s1->setName("Richard");
    s1->setEmail("richard@gmail.com");
    s1->setBirth("01/02/1998");
    s1->setGender('m');
    s1->setPassword("1234");
    s1->setFavorites(v);
    QVERIFY(s1->getName() == "Richard");
    QVERIFY(s1->getEmail() == "richard@gmail.com");
    QVERIFY(s1->getBirth() == "01/02/1998");
    QVERIFY(s1->getGender() == 'm');
    QVERIFY(s1->getPassword() == "1234");
    QVERIFY(s1->getFavorites() == v);
    delete s1;
}

void testSeriesFeelings::unit_user_comparison(){
    User *s1 = new UserImpl();
    vector<bool> v;
    s1->setName("Richard");
    s1->setEmail("richard@gmail.com");
    s1->setBirth("01/02/1998");
    s1->setGender('m');
    s1->setPassword("1234");
    s1->setFavorites(v);
    User *s2 = new UserImpl();
    s2->setName("Richard");
    s2->setEmail("richard@gmail.com");
    s2->setBirth("01/02/1998");
    s2->setGender('m');
    s2->setPassword("1234");
    s2->setFavorites(v);
    QCOMPARE (s1->getName(), s2->getName());
    QCOMPARE (s1->getEmail(), s2->getEmail());
    QCOMPARE (s1->getBirth(), s2->getBirth());
    QCOMPARE (s1->getGender(), s2->getGender());
    QCOMPARE (s1->getPassword(), s2->getPassword());
    QCOMPARE (s1->getFavorites(), s2->getFavorites());
    delete s1; delete s2;
}

void testSeriesFeelings::unit_comment_constructor()
{
    Comment* comment1 = new CommentImpl();
    QVERIFY(comment1->getComment() == "");
    User* user1 = new UserImpl();
    Comment* comment2 = new CommentImpl("Muito boa!", user1);
    QVERIFY(comment2->getComment() == "Muito boa!");
    QVERIFY(comment2->getUser() == user1);
    delete comment1; delete comment2; delete user1;
}

void testSeriesFeelings::unit_comment_operator()
{
    Comment* comment1 = new CommentImpl();
    comment1->setComment("top");
    Comment* comment2 = new CommentImpl();
    (*comment2) = (*comment1);
    QVERIFY(comment1 != comment2);
    QCOMPARE(comment1->getComment(), comment2->getComment());
    delete comment1; delete comment2;
}

void testSeriesFeelings::unit_comment_copyTest()
{
    Comment* comment1 = new CommentImpl();
    comment1->setComment("legal");
    Comment* comment2 = new CommentImpl(comment1);
    QVERIFY(comment1 != comment2 && comment1->getComment() == comment2->getComment());
    delete comment1; delete comment2;
}

void testSeriesFeelings::unit_comment_assignTest()
{
    User *user1 = new UserImpl();
    Comment* comment1 = new CommentImpl();
    string str1 = "Inesquecivel!";
    comment1->setUser(user1);
    comment1->setComment(str1);
    QVERIFY(comment1->getComment() == "Inesquecivel!");
    QVERIFY(comment1->getUser() == user1);
    delete user1; delete comment1;
}

void testSeriesFeelings::unit_serie_constructor()
{
    Serie* serie1 = new SerieImpl();
    serie1->setName("Harry Poter");
    vector <Comment*> comments;
    Comment* comment1 = new CommentImpl();
    comment1->setComment("top");
    comments.push_back(comment1);
    serie1->setCommentList(comments);
    QVERIFY(serie1->getName() == "Harry Poter");
    QVERIFY(serie1->getCommentList() == comments);
    delete comment1; delete serie1;
}

void testSeriesFeelings::unit_serie_operator()
{
    Serie* serie1 = new SerieImpl();
    serie1->setName("Suits");
    Serie* serie2 = new SerieImpl();
    (*serie2) = (*serie1);
    QVERIFY(serie1 != serie2);
    QCOMPARE(serie1->getName(), serie2->getName());
    delete serie1; delete serie2;
}

void testSeriesFeelings::unit_serie_copyTest()
{
    Serie* serie1 = new SerieImpl();
    serie1->setName("Suits");
    Serie* serie2 = new SerieImpl(serie1);
    QVERIFY(serie1 != serie2 && serie1->getName() == serie2->getName());
    delete serie1; delete serie2;
}

void testSeriesFeelings::unit_serie_assignTest()
{
    Serie* serie1 = new SerieImpl();
    serie1->setName("Harry Poter");
    vector <Comment*> comments;
    Comment* comment1 = new CommentImpl();
    User *user1 = new UserImpl();
    comment1->setUser(user1);
    comment1->setComment("top");
    comments.push_back(comment1);
    serie1->setCommentList(comments);
    QVERIFY(serie1->getName() == "Harry Poter");
    QVERIFY(serie1->getCommentList() == comments);
    delete serie1; delete comment1; delete user1;
}

void testSeriesFeelings::unit_model_constructor()
{
    Model* model1 = Model::createModel();
    QVERIFY(model1 != nullptr);
    Model* model2 = new ModelImpl();
    QVERIFY(model2 != nullptr);
    delete model1; delete model2;
}

void testSeriesFeelings::unit_model_assignTest()
{
    Model* model1 = new ModelImpl();
    User* user1 = new UserImpl();
    Serie* serie1 = new SerieImpl();
    vector<User*> userList;
    vector<Serie*> serieList;
    userList.push_back(user1);
    model1->setUser(user1);
    model1->setUserList(userList);
    model1->setSerieList(serieList);
    QVERIFY(model1->getSerieList() == serieList);
    QVERIFY(model1->getUserList() == userList);
    QVERIFY(model1->getUser() == user1);
    delete model1; delete user1; delete serie1;
}

void testSeriesFeelings::unit_register_constructor()
{
    Register* reg1 = new RegisterImpl();
    QVERIFY(reg1 != nullptr);
    delete reg1;
    Register* reg2 = Register::createRegister();
    QVERIFY(reg2 != nullptr);
    delete reg2;
}

void testSeriesFeelings::unit_register_assignTest()
{
    Model* model1 = new ModelImpl();
    User* user1 = new UserImpl();
    Register* reg1 = new RegisterImpl();
    vector<bool> favorites {true, true, false, false, false, true};
    reg1->create(model1, "Ricardo", "ric@gmail.com", "01/03/1998", 'm', "123", favorites, 0);
    bool find = reg1->search(model1, "ric@gmail.com");
    QCOMPARE(find, true);
    reg1->remove(model1, reg1->consult(model1, "ric@gmail.com"));
    find = reg1->search(model1, "ric@gmail.com");
    QCOMPARE(find, false);
    delete model1; delete user1; delete reg1;
}

QTEST_MAIN(testSeriesFeelings)
#include "tst_testseriesfeelings.moc"



