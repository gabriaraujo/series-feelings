#ifndef MODEL_H
#define MODEL_H

#include "../classes/serie.h"

/**
 * @brief represents a class that works as a factory
 * 
 */
class Model {
public:
    /**
     * @brief Destroy the Model object
     * 
     */
    virtual ~Model() {};

    /**
     * @brief Create a Model object
     * 
     * @return Model* the model pointer
     */
    static Model* createModel();

    /**
     * @brief Create a User object
     * 
     * @return User* the user pointer
     */
    virtual User* createUser(
        string, string, string, char, string, const vector<bool>&, int
    ) = 0;

    /**
     * @brief Create a Comment object
     *
     * @return Comment* the comment pointer
     */
    virtual Comment* createComment(User*, string) = 0;

    /**
     * @brief Create a Serie object
     *
     * @return Serie* the serie pointer
     */
    virtual Serie* createSerie(string, const vector<Comment*>&) = 0;

    /**
     * @brief Get the User List object
     * 
     * @return vector<User*>& the user list
     */
    virtual vector<User*>& getUserList() = 0;

    /**
     * @brief Set the User List object
     * 
     */
    virtual void setUserList(const vector<User*> &) = 0;

    /**
     * @brief Get the Serie List object
     *
     * @return vector<Serie*>& the serie list
     */
    virtual vector<Serie*>& getSerieList() = 0;

    /**
     * @brief Set the Serie List object
     *
     */
    virtual void setSerieList(const vector<Serie*>&) = 0;

    /**
     * @brief Get the User object
     * 
     * @return User* the user pointer
     */
    virtual User *getUser() const = 0;

    /**
     * @brief Set the User object
     * 
     */
    virtual void setUser(User*) = 0;

    /**
     * @brief read a list of users from a .csv file
     * 
     * @return true  if it was successful
     * @return false if it failed
     */
    virtual bool readUser(const string&) = 0;

    /**
     * @brief write a list of users in a .csv file
     * 
     * @return true  if it was successful
     * @return false if it failed 
     */
    virtual bool writeUser(const string&) = 0;

    /**
     * @brief read a list of comments from a .csv file
     *
     * @return true  if it was successful
     * @return false if it failed
     */
    virtual bool readComments(const string&) = 0;

    /**
     * @brief write a list of comments in a .csv file
     *
     * @return true  if it was successful
     * @return false if it failed
     */
    virtual bool writeComments(const string&) = 0;

    /**
     * @brief read a list of series from a .csv file
     *
     * @return true  if it was successful
     * @return false if it failed
     */
    virtual bool readSeries(const string&) = 0;
};

#endif // MODEL_H
