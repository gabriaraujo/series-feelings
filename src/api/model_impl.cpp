#include "model_impl.h"

ModelImpl::ModelImpl() {
    this->user = nullptr;
}

ModelImpl::~ModelImpl() {}

Model* Model::createModel() {
    return ModelImpl::createModel();
}

Model* ModelImpl::createModel() {
    Model* model = new ModelImpl();
    model->setUser(nullptr);

    model->readUser("../series-feelings/database/user-list.csv");
    model->readSeries("../series-feelings/database/series.txt");
    model->readComments("../series-feelings/database/comments.csv");

    return model;
}

User* ModelImpl::createUser(
    string name,
    string email,
    string birth,
    char gender,
    string password,
    const vector<bool>& favorites,
    int permission
) {
    User* user = new UserImpl(
        name, email, birth, gender, password, favorites, permission
    );

    this->user_list.push_back(user);

    return user;
}

Comment* ModelImpl::createComment(User* user, string comment) {
    return new CommentImpl(comment, user);
}

Serie* ModelImpl::createSerie(string name, const vector<Comment*>& list) {
    return new SerieImpl(name, list);
}

User *ModelImpl::getUser() const {
    return user;
}

void ModelImpl::setUser(User *value) {
    user = value;
}

vector<User*>& ModelImpl::getUserList() {
    return user_list;
}

void ModelImpl::setUserList(const vector<User*> &value) {
    user_list = value;
}

vector<Serie*>& ModelImpl::getSerieList() {
    return serie_list;
}

void ModelImpl::setSerieList(const vector<Serie*>& value) {
    serie_list = value;
}

bool ModelImpl::readUser(const string& file){
    ifstream input_file;
    vector<User*> user_list;

    input_file.open(file, ios::in);

    string name, email, birth, gender, password, fav_1, fav_2, fav_3, fav_4, fav_5, fav_6, permission;
    vector<string> favorites_aux;
    while(getline(input_file, name, ';')){
        getline(input_file, email, ';');

        getline(input_file, birth, ';');

        getline(input_file, gender, ';');

        getline(input_file, password, ';');

        getline(input_file, fav_1, ';');
        favorites_aux.push_back(fav_1);

        getline(input_file, fav_2, ';');
        favorites_aux.push_back(fav_2);

        getline(input_file, fav_3, ';');
        favorites_aux.push_back(fav_3);

        getline(input_file, fav_4, ';');
        favorites_aux.push_back(fav_4);

        getline(input_file, fav_5, ';');
        favorites_aux.push_back(fav_5);

        getline(input_file, fav_6, ';');
        favorites_aux.push_back(fav_6);

        getline(input_file, permission);

        vector<bool> favorites;
        for(auto it : favorites_aux){
            if(it == "0")
                favorites.push_back(false);
            else
                favorites.push_back(true);
        }

        User* user = this->createUser(
            name, email, birth, gender[0], password, favorites, stoi(permission)
        );
        user_list.push_back(user);
    }

    this->setUserList(user_list);

    input_file.close();

    return input_file.good();
}

bool ModelImpl::writeUser(const string& file){
    ofstream output_file;

    output_file.open(file, ios::out);

    for (auto it : this->user_list) {
        output_file << it->getName() << ";"
                    << it->getEmail() << ";"
                    << it->getBirth() << ";"
                    << it->getGender() << ";"
                    << it->getPassword() << ";";
        for (auto fav : it->getFavorites())
            output_file << fav << ";";

        output_file << it->getPermission() << "\n";
    }

    output_file.close();

    return output_file.good();
}

bool ModelImpl::readComments(const string& file){
    ifstream input_file;

    input_file.open(file, ios::in);

    string serie_name, user_email, comment;
    Register* reg = Register::createRegister();

    while (getline(input_file, serie_name, ';')) {
        getline(input_file, user_email, ';');
        getline(input_file, comment);

        for (auto it : this->serie_list)
            if(it->getName() == serie_name){
                User* user = reg->consult(this, user_email);
                Comment* c = this->createComment(user, comment);
                it->getCommentList().push_back(c);
            }
    }

    input_file.close();

    return input_file.good();
}

bool ModelImpl::writeComments(const string& file){
    ofstream output_file;

    output_file.open(file, ios::out);

    for (auto it : this->serie_list)
        for(auto i : it->getCommentList()){
            output_file << it->getName() << ";" << i->getUser()->getEmail() << ";" << i->getComment() << "\n";
        }

    output_file.close();

    return output_file.good();
}

bool ModelImpl::readSeries(const string& file){
    ifstream input_file;

    input_file.open(file, ios::in);

    string serie_name;

    while (getline(input_file, serie_name)) {
        vector<Comment*> comments;
        Serie* serie = this->createSerie(serie_name, comments);
        this->serie_list.push_back(serie);
    }

    input_file.close();

    return input_file.good();
}
