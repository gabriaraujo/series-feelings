#include "register_impl.h"

#include <iostream>
using namespace std;

RegisterImpl::RegisterImpl() {}

RegisterImpl::~RegisterImpl() {}

Register* Register::createRegister() {
    return RegisterImpl::createRegister();
}

Register* RegisterImpl::createRegister() {
    return new RegisterImpl();
}

bool RegisterImpl::create(
    Model* model,
    string name,
    string email,
    string birth,
    char gender,
    string password,
    const vector<bool>& favorites,
    int permission
) {
    User* user = model->createUser(
        name, email, birth, gender, password, favorites, permission
    );
    model->setUser(user);
    return model->writeUser("../series-feelings/database/user-list.csv");
}

bool RegisterImpl::remove(Model* model, User* user) {
    if(!this->search(model, user->getEmail()))
        return false;
    else{
        model->getUserList().erase(
            std::remove(
                model->getUserList().begin(),
                model->getUserList().end(), user
            ),
            model->getUserList().end()
        );
        model->setUser(nullptr);

        for (auto it : model->getSerieList())
            for (auto c : it->getCommentList())
                this->remove(model, it, user, c);

        return model->writeUser("../series-feelings/database/user-list.csv");
    }
}

void RegisterImpl::update(
    Model* model,
    User* user,
    string name,
    string email,
    string birth,
    char gender,
    string password,
    const vector<bool>& favorites
) {
    user->setName(name);
    user->setEmail(email);
    user->setBirth(birth);
    user->setGender(gender);
    user->setPassword(password);
    user->setFavorites(favorites);
    model->writeUser("../series-feelings/database/user-list.csv");
}

bool RegisterImpl::search(Model* model, const string& email) const {
    for(auto it : model->getUserList())
        if(it->getEmail() == email)
            return true;

    return false;
}

User* RegisterImpl::consult(Model* model, const string& email) const {
    for(auto it : model->getUserList())
        if(it->getEmail() == email)
            return it;

    return nullptr;
}

bool RegisterImpl::ValidateEmail(string email) {
    if (regex_match(email, regex("([a-z]+)([_.a-z0-9]*)([a-z0-9]+)(@)([a-z]+)([.a-z]+)([a-z]+)")))
        return true;

    return false;
}

Comment* RegisterImpl::create(Model* model, User* user, string comment) {
    return model->createComment(user, comment);
};

bool RegisterImpl::remove(Model* model, Serie* serie, User* user, Comment* comment) {
    for (auto com : serie->getCommentList())
        if (com->getUser()->getEmail() == user->getEmail()
        && comment->getComment() == com->getComment()) {
            serie->getCommentList().erase(
                std::remove(
                    serie->getCommentList().begin(),
                    serie->getCommentList().end(), com
                ),
                serie->getCommentList().end()
            );

            return model->writeComments("../series-feelings/database/comments.csv");
        }

    return false;
};

void RegisterImpl::update(Model* model, User* user, Comment* comment, string text) {
    if (comment->getUser()->getEmail() == user->getEmail())
        comment->setComment(text);

    model->writeComments("../series-feelings/database/comments.csv");
};
