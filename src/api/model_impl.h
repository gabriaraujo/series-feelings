#ifndef MODEL_IMPL_H
#define MODEL_IMPL_H

#include <fstream>
#include <sstream>
#include "model.h"
#include "../classes/user_impl.h"
#include "../classes/comment_impl.h"
#include "../classes/serie_impl.h"
#include "register.h"

using std::ofstream;
using std::ifstream;
using std::getline;
using std::stringstream;
using std::ios;

#include <iostream>
using namespace std;

/**
 * @brief this class represents the implementation of the model
 * 
 */
class ModelImpl : public Model {
protected:
    vector<User*> user_list;    /**< The list with stored users. */
    vector<Serie*> serie_list;  /**< The list with stored series. */
    User* user;                 /**< The logged user. */

public:
    /**
     * @brief Construct a new Model Impl object
     * 
     */
    ModelImpl();

    /**
     * @brief Destroy the Model Impl object
     * 
     */
    ~ModelImpl();

    /**
     * @brief Create a Model object
     * 
     * @return Model* the model pointer
     */
    static Model* createModel();

    /**
     * @brief Create a User object
     * 
     * @return User* the user pointer
     */
    User* createUser(
        string, string, string, char, string, const vector<bool>&, int
    );

    /**
     * @brief Create a Comment object
     *
     * @return User* the user pointer
     */
    Comment* createComment(User*, string);

    /**
     * @brief Create a Serie object
     *
     * @return Serie* the serie pointer
     */
    Serie* createSerie(string, const vector<Comment*>&);

    /**
     * @brief Get the User List object
     *
     * @return vector<User*>& the user list
     */
    vector<User*>& getUserList();

    /**
     * @brief Set the User List object
     * 
     */
    void setUserList(const vector<User*> &);

    /**
     * @brief Get the Serie List object
     *
     * @return vector<Serie*>& the serie list
     */
    vector<Serie*>& getSerieList();

    /**
     * @brief Set the Serie List object
     *
     */
    void setSerieList(const vector<Serie*>&);

    /**
     * @brief Get the User object
     * 
     * @return User* the user pointer
     */
    User *getUser() const;

    /**
     * @brief Set the User object
     * 
     * @param value the user pointer
     */
    void setUser(User *value);

    /**
     * @brief read a list of users from a .csv file
     * 
     * @return true  if it was successful
     * @return false if it failed
     */
    bool readUser(const string&);

    /**
     * @brief write a list of users in a .csv file
     * 
     * @return true  if it was successful
     * @return false if it failed 
     */
    bool writeUser(const string&);

    /**
     * @brief read a list of comments from a .csv file
     *
     * @return true  if it was successful
     * @return false if it failed
     */
    bool readComments(const string&);

    /**
     * @brief write a list of comments in a .csv file
     *
     * @return true  if it was successful
     * @return false if it failed
     */
    bool writeComments(const string&);

    /**
     * @brief read a list of series from a .csv file
     *
     * @return true  if it was successful
     * @return false if it failed
     */
    bool readSeries(const string&);
};

#endif // MODEL_IMPL_H
