#ifndef SERIEIMPL_H
#define SERIEIMPL_H

#include "serie.h"

/**
 * @brief This class represents a Serie
 * 
 */
class SerieImpl : public Serie {
protected:
    string name;                    /**< The serie name. */
    vector<Comment*> comment_list;  /**< The comment list. */

public:
    /**
     * @brief Construct a new Serie Impl object
     * 
     */
    SerieImpl();

    /**
     * @brief Construct a new Serie Impl object
     * 
     */
    SerieImpl(Serie*);

    /**
     * @brief Construct a new Serie Impl object
     * 
     */
    SerieImpl(string, const vector<Comment*>&);

    /**
     * @brief Destroy the Serie Impl object
     * 
     */
    ~SerieImpl();

    /**
     * @brief Get the Name object
     * 
     * @return string the serie name
     */
    string getName() const;

    /**
     * @brief Set the Name object
     * 
     */
    void setName(const string &);

    /**
     * @brief Get the Comment List object
     * 
     * @return vector<Comment*> the serie list
     */
    vector<Comment*>& getCommentList();

    /**
     * @brief Set the Comment List object
     * 
     */
    void setCommentList(const vector<Comment*>&);

    /**
     * @brief assignment operator
     *
     * @return Serie&
     */
    Serie& operator=(Serie&);
};

#endif // SERIEIMPL_H
