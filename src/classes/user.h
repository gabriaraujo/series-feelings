#ifndef USER_H
#define USER_H

#include <string>
#include <vector>

using std::string;
using std::vector;

/**
 * @brief this class represents an user
 * 
 */
class User {
public:
    /**
     * @brief Destroy the User object
     * 
     */
    virtual ~User() {};

    /**
     * @brief Get the Name object
     * 
     * @return string the user name
     */
    virtual string getName() const = 0;

    /**
     * @brief Set the Name object
     * 
     */
    virtual void setName(const string &) = 0;

    /**
     * @brief Get the Email object
     * 
     * @return string the user email
     */
    virtual string getEmail() const = 0;

    /**
     * @brief Set the Email object
     * 
     */
    virtual void setEmail(const string &) = 0;

    /**
     * @brief Get the Birth object
     * 
     * @return string the user birthday
     */
    virtual string getBirth() const = 0;

    /**
     * @brief Set the Birth object
     * 
     */
    virtual void setBirth(const string &) = 0;

    /**
     * @brief Get the Gender object
     * 
     * @return char the user gender
     */
    virtual char getGender() const = 0;

    /**
     * @brief Set the Gender object
     * 
     */
    virtual void setGender(char) = 0;

    /**
     * @brief Get the Password object
     * 
     * @return string the user password
     */
    virtual string getPassword() const = 0;

    /**
     * @brief Set the Password object
     * 
     */
    virtual void setPassword(const string &) = 0;

    /**
     * @brief Get the Favorites object
     * 
     * @return vector<bool>& the user favorites
     */
    virtual vector<bool>& getFavorites() = 0;

    /**
     * @brief Set the Favorites object
     * 
     */
    virtual void setFavorites(const vector<bool> &) = 0;

    /**
     * @brief Get the Permission object
     * 
     * @return int the user permission level
     */
    virtual int getPermission() const = 0;

    /**
     * @brief Set the Permission object
     * 
     */
    virtual void setPermission(int) = 0;

    /**
     * @brief assignment operator
     * 
     * @return User& the user reference
     */
    virtual User& operator=(User&) = 0;

    /**
     * @brief association operator
     * 
     * @return true  if it was successful
     * @return false if it failed 
     */
    virtual bool operator==(User&) = 0;
};

#endif // USER_H
