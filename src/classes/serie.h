#ifndef SERIE_H
#define SERIE_H

#include "comment.h"

/**
 * @brief This class represents a Serie
 * 
 */
class Serie {
public:
    /**
     * @brief Destroy the Serie object
     * 
     */
    virtual ~Serie() {};

    /**
     * @brief Get the Name object
     * 
     * @return string the serie name
     */
    virtual string getName() const = 0;

    /**
     * @brief Set the Name object
     * 
     */
    virtual void setName(const string&) = 0;

    /**
     * @brief Get the Comment List object
     * 
     * @return vector<Comment*> the comment list
     */
    virtual vector<Comment*>& getCommentList() = 0;

    /**
     * @brief Set the Comment List object
     * 
     */
    virtual void setCommentList(const vector<Comment*>&) = 0;

    /**
     * @brief assignment operator
     *
     * @return Serie& the serie reference
     */
    virtual Serie& operator=(Serie&) = 0;
};

#endif // SERIE_H
