#ifndef USER_IMPL_H
#define USER_IMPL_H

#include "user.h"

/**
 * @brief this class represents an user
 * 
 */
class UserImpl : public User {
protected:
    string name;                /**< the user name. */
    string email;               /**< the user email. */
    string birth;               /**< the user birthday. */
    char gender;                /**< the user gender. */
    string password;            /**< the user password. */
    vector<bool> favorites;     /**< the user favorites. */
    int permission;             /**< the user permission. */

public:
    /**
     * @brief Construct a new User Impl object
     * 
     */
    UserImpl();

    /**
     * @brief Construct a new User Impl object
     * 
     */
    UserImpl(User*);

    /**
     * @brief Construct a new User Impl object
     * 
     */
    UserImpl(string, string, string, char, string, const vector<bool>&, int);

    /**
     * @brief Destroy the User Impl object
     * 
     */
    ~UserImpl();

    /**
     * @brief Get the Name object
     * 
     * @return string the user name
     */
    string getName() const;

    /**
     * @brief Set the Name object
     * 
     */
    void setName(const string &);

    /**
     * @brief Get the Email object
     * 
     * @return string the user email
     */
    string getEmail() const;

    /**
     * @brief Set the Email object
     * 
     */
    void setEmail(const string &);

    /**
     * @brief Get the Birth object
     * 
     * @return string 
     */
    string getBirth() const;

    /**
     * @brief Set the Birth object
     * 
     */
    void setBirth(const string &);

    /**
     * @brief Get the Gender object
     * 
     * @return char the user gender
     */
    char getGender() const;

    /**
     * @brief Set the Gender object
     * 
     */
    void setGender(char);

    /**
     * @brief Get the Password object
     * 
     * @return string the user password
     */
    string getPassword() const;

    /**
     * @brief Set the Password object
     * 
     */
    void setPassword(const string &);

    /**
     * @brief Get the Favorites object
     * 
     * @return vector<bool>& the user favorites
     */
    vector<bool>& getFavorites();

    /**
     * @brief Set the Favorites object
     * 
     */
    void setFavorites(const vector<bool> &);

    /**
     * @brief Get the Permission object
     * 
     * @return int the user permission level
     */
    int getPermission() const;

    /**
     * @brief Set the Permission object
     * 
     */
    void setPermission(int);

    /**
     * @brief assignment operator
     * 
     * @return User& 
     */
    User& operator=(User&);

    /**
     * @brief association operator
     * 
     * @return true  if it was successful
     * @return false if it failed 
     */
    bool operator==(User&);
};

#endif // USER_IMPL_H
