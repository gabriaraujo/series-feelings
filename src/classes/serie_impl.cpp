#include "serie_impl.h"

SerieImpl::SerieImpl() {}

SerieImpl::SerieImpl(Serie* serie) {
    this->name = serie->getName();
    this->comment_list = serie->getCommentList();
}

SerieImpl::SerieImpl(string name, const vector<Comment*>& comments) {
    this->name = name;
    this->comment_list = comments;
}

SerieImpl::~SerieImpl() {}

string SerieImpl::getName() const {
    return name;
}

void SerieImpl::setName(const string &value) {
    name = value;
}

vector<Comment*>& SerieImpl::getCommentList() {
    return comment_list;
}

void SerieImpl::setCommentList(const vector<Comment *> &value) {
    comment_list = value;
}

Serie& SerieImpl::operator=(Serie& s1) {
    if (this == &s1)
        return *this;

    this->name = s1.getName();
    this->comment_list = s1.getCommentList();
    return *this;
}
