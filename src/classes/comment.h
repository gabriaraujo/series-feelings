#ifndef COMMENT_H
#define COMMENT_H

#include "user.h"

/**
 * @brief this class represents an comment
 * 
 */
class Comment {
public:
    /**
     * @brief Destroy the Comment object
     * 
     */
    virtual ~Comment() {};

    /**
     * @brief Get the Comment object
     * 
     * @return string 
     */
    virtual string getComment() const = 0;

    /**
     * @brief Set the Comment object
     * 
     */
    virtual void setComment(const string &) = 0;

    /**
     * @brief Get the User object
     * 
     * @return User* 
     */
    virtual User *getUser() const = 0;

    /**
     * @brief Set the User object
     * 
     * @param value 
     */
    virtual void setUser(User *value) = 0;

    /**
     * @brief assignment operator
     *
     * @return Comment& the comment reference
     */
    virtual Comment& operator=(Comment&) = 0;
};

#endif // COMMENT_H
