#ifndef COMMENTIMPL_H
#define COMMENTIMPL_H

#include "comment.h"

/**
 * @brief this class represents an comment
 * 
 */
class CommentImpl : public Comment{
protected:
    string comment;                /**< the comment. */
    User* user;                    /**< the user who made the comment. */

public:
    /**
     * @brief Construct a new Comment Impl object
     * 
     */
    CommentImpl();

    /**
     * @brief Construct a new Comment Impl object
     * 
     */
    CommentImpl(Comment*);

    /**
     * @brief Construct a new Comment Impl object
     * 
     */
    CommentImpl(string, User*);

    /**
     * @brief Destroy the Comment Impl object
     * 
     */
    ~CommentImpl();

    /**
     * @brief Get the Comment object
     * 
     * @return string 
     */
    string getComment() const;

    /**
     * @brief Set the Comment object
     * 
     * @param value 
     */
    void setComment(const string &value);

    /**
     * @brief Get the User object
     * 
     * @return User* 
     */
    User *getUser() const;

    /**
     * @brief Set the User object
     * 
     * @param value 
     */
    void setUser(User *value);

    /**
     * @brief assignment operator
     *
     * @return Comment&
     */
    Comment& operator=(Comment&);
};

#endif // COMMENTIMPL_H
