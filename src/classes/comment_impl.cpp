#include "comment_impl.h"

CommentImpl::CommentImpl() {}

CommentImpl::CommentImpl(Comment* comment){
    this->comment = comment->getComment();
    this->user = comment->getUser();
}

CommentImpl::CommentImpl(string comment, User* user){
    this->comment = comment;
    this->user = user;
}

CommentImpl::~CommentImpl(){

}

string CommentImpl::getComment() const
{
    return comment;
}

void CommentImpl::setComment(const string &value)
{
    comment = value;
}

User *CommentImpl::getUser() const
{
    return user;
}

void CommentImpl::setUser(User *value)
{
    user = value;
}

Comment& CommentImpl::operator=(Comment& comment) {
    if (this == &comment)
        return *this;

    this->user = comment.getUser();
    this->comment = comment.getComment();
    return *this;
}
