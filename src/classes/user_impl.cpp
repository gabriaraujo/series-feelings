#include "user_impl.h"

UserImpl::UserImpl() {}

UserImpl::UserImpl(User* user) {
    this->name = user->getName();
    this->email = user->getEmail();
    this->birth = user->getBirth();
    this->gender = user->getGender();
    this->password = user->getPassword();
    this->favorites = user->getFavorites();
}

UserImpl::UserImpl(
    string name,
    string email,
    string birth,
    char gender,
    string password,
    const vector<bool>& favorites,
    int permission
) {
    this->name = name;
    this->email = email;
    this->birth = birth;
    this->gender = gender;
    this->password = password;
    this->favorites = favorites;
    this->permission = permission;
}

UserImpl::~UserImpl() {}

string UserImpl::getName() const {
    return name;
}

void UserImpl::setName(const string &value) {
    name = value;
}

string UserImpl::getEmail() const {
    return email;
}

void UserImpl::setEmail(const string &value) {
    email = value;
}

string UserImpl::getBirth() const {
    return birth;
}

void UserImpl::setBirth(const string &value) {
    birth = value;
}

char UserImpl::getGender() const {
    return gender;
}

void UserImpl::setGender(char value) {
    gender = value;
}

string UserImpl::getPassword() const {
    return password;
}

void UserImpl::setPassword(const string &value) {
    password = value;
}

vector<bool>& UserImpl::getFavorites() {
    return favorites;
}

void UserImpl::setFavorites(const vector<bool> &value) {
    favorites = value;
}

int UserImpl::getPermission() const {
    return permission;
}

void UserImpl::setPermission(int value) {
    permission = value;
}

User& UserImpl::operator=(User& user) {
    if (this == &user)
        return *this;

    this->name = user.getName();
    this->email = user.getEmail();
    this->birth = user.getBirth();
    this->gender = user.getGender();
    this->password = user.getPassword();
    this->favorites = user.getFavorites();
    this->permission = user.getPermission();

    return *this;
}

bool UserImpl::operator==(User& rhs) {
    return this->name == rhs.getName()
        && this->email == rhs.getEmail()
        && this->birth == rhs.getBirth()
        && this->gender == rhs.getGender()
        && this->password == rhs.getPassword()
        && this->favorites == rhs.getFavorites()
        && this->permission == rhs.getPermission();
}
