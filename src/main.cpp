#include "views/homepage.h"
#include "views/commentview.h"

#include <QApplication>

int main(int argc, char *argv[]) {
    QApplication a(argc, argv);

    Model* model = Model::createModel();

    RegisterView* rv = new RegisterView();
    rv->setModel(model);

    HomePage* home = new HomePage();
    home->setModel(model);

    AddComment* ac = new AddComment();
    ac->setModel(model);

    EditComment* ec = new EditComment();
    ec->setModel(model);

    CommentView* c = new CommentView();
    c->setModel(model);
    c->setEditComment(ec);

    ProfileEdit* pe = new ProfileEdit();
    pe->setModel(model);

    Login* login = new Login();
    login->setReg(rv);
    login->setModel(model);

    SerieView* series = new SerieView();
    series->setModel(model);
    series->setComments(c);
    series->setAddComment(ac);

    home->setSeries(series);
    home->setLogin(login);
    home->setProfileEdit(pe);

    return home->exec();
}
